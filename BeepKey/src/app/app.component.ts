import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LANG_EN, TranslationService } from './shared/services/translation.service';
import { filter, map } from 'rxjs';
import * as AOS from 'aos';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'BeepKey';
  lang: string;

  constructor(
    private translate: TranslateService,
    private translation: TranslationService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title
  ) {
    translation.langEE.subscribe((res) => this.setupLanguage(res));
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map(() => {
        let child = this.activatedRoute.firstChild;
        while (child) {
          if (child.firstChild) {
            child = child.firstChild;
          } else if (child.snapshot.data && child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          } else {
            return null;
          }
        }
        return null;
      })
    ).subscribe((data: any) => {
      if (data) {
        translate.get(data).subscribe(res => {
          this.titleService.setTitle(res + ' - BeepKey');
        })
      }
    });

  }
  ngOnInit(): void {
    this.lang = this.translation.language || LANG_EN;
    this.setupLanguage(this.lang);
    AOS.init();
  }

  private setupLanguage(lang: string) {
    this.translate.setDefaultLang(lang);
    this.translate.use(lang);
  }
}
