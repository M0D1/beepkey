import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map, Observable, of, tap } from 'rxjs';
import { LoginCredentials, LoginResponse } from '../../shared/models/auth.model';
import { Company } from '../../shared/models/company.model';
import { Profile } from '../../shared/models/profile.model';
import { RawToken } from '../../shared/models/row.token';
import { User } from '../../shared/models/user.model';
import { UrlBuilder } from '../../shared/utils/url.builder';
export const TOKEN = 'token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private user$: User;

  constructor(
    public jwtHelper: JwtHelperService,
    private url: UrlBuilder,
    private http: HttpClient
  ) { }
  get myCompany(): Company {
    return this.user.company || null;
  }
  get myProfile(): Profile {
    return this.user.profile;
  }

  get user(): User {
    if (this.jwt && !this.user$) {
      this.fetchMe();
    }
    return this.user$;
  }

  get jwt(): string {
    return localStorage.getItem(TOKEN);
  }

  set jwt(jwt: string) {
    if (jwt) {
      localStorage.setItem(TOKEN, jwt);
    }
  }

  get accessHeader(): HttpHeaders {
    return new HttpHeaders({
      Authorization: `Bearer ${this.jwt}`,
    });
  }

  get decodedJwt(): RawToken {
    return this.jwtHelper.decodeToken(this.jwt) as RawToken;
  }

  login(credentials: LoginCredentials): Observable<any> {
    const { email, password } = credentials;
    const body = { email, password };
    return this.http
      .post<LoginResponse>(`${this.url.auth('login')}`, body)
      .pipe(
        tap((res) => {
          this.setAccessToken(res.token);
          res ? this.fetchMe() : null;
        })
      );
  }

  isAuthenticated(): boolean {
    return !this.jwtHelper.isTokenExpired(this.jwt) || false;
  }

  logout() {
    localStorage.removeItem(TOKEN);
  }

  hasAccess(): Observable<boolean> {
    if (this.jwt) {
      return of(this.isAuthenticated()).pipe(
        map((res) => res),
        tap((res) => (!res ? localStorage.removeItem(TOKEN) : null))
      );
    } else {
      return of(false);
    }
  }

  private setAccessToken(value: string) {
    try {
      this.jwt = value;
    } catch (error) {
      console.log(`Error ${error}`);
    }
  }

  private fetchMe() {
    if (this.jwt)
      this.http.get<User>(
        this.url.user('me'),
        { headers: this.accessHeader }
      ).subscribe((res) => this.user$ = res);
  }
}
