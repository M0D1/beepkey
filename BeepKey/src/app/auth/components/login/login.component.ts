import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginFG: FormGroup;
  loginProgress: boolean = false;
  constructor(
    private auth: AuthService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.loginFG = this.fb.group({
      email: ['email@domain.com', Validators.compose([Validators.email, Validators.required])],
      password: ['xxxxxxxx', Validators.compose([Validators.required, Validators.minLength(8)])]
    })
  }

  get email(): AbstractControl {
    return this.loginFG.get('email')
  }
  
  get password(): AbstractControl {
    return this.loginFG.get('password')
  }

  login() {
    if (this.loginFG.valid) {
      this.auth.login({ email: this.email.value, password: this.password.value }).subscribe(res => {
        console.log(res);
        this.loginProgress = false;
      })
    }
  }
}
