import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

const API = env.api;
@Injectable({
    providedIn: 'root',
})
export class UrlBuilder {

    constructor() {
    }
    auth(path: string = ''): string { return `${this.auth$(path)}`; }
    user(sid: string = ''): string { return `${this.user$(sid)}`; }
    company(sid: string = ''): string { return `${this.company$(sid)}`; }
    job(sid: string = ''): string { return `${this.job$(sid)}`; }

    private auth$(path: string = ''): string { return `${API}/auth${path !== '' ? `/${path}` : ''}`; }
    private user$(sid: string = ''): string { return `${API}/user${sid !== '' ? `/${sid}` : ''}`; }
    private job$(sid: string = ''): string { return `${API}/job${sid !== '' ? `/${sid}` : ''}`; }
    private company$(sid: string = '') { return `${API}/company${sid !== '' ? `/${sid}` : ''}` }

}