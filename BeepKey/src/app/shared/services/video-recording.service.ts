import { Injectable } from '@angular/core';
import * as RecordRTC from 'recordrtc';
import * as moment from "moment";
import { Observable, Subject } from 'rxjs';
interface RecordedVideoOutput {
  blob: Blob;
  url: string;
  title: string;
}
export interface RecordProgress {
  progress: number;
  final: number;
}

@Injectable({
  providedIn: 'root'
})
export class VideoRecordingService {


  private stream: MediaStream | HTMLCanvasElement | HTMLVideoElement | HTMLElement | any;
  private recorder: RecordRTC;
  private interval;
  private startTime;
  private _stream = new Subject<MediaStream>();
  private _recorded = new Subject<RecordedVideoOutput>();
  private _recordedUrl = new Subject<string>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<string>();
  private _videoLimit = new Subject<RecordProgress>();
  private _maxRecordTime: number = 0;

  get maxRecordTime() {
    return this._maxRecordTime;
  }

  set maxRecordTime(length: number) {
    this._maxRecordTime = length;
  }

  get RecordedUrl(): Observable<string> {
    return this._recordedUrl.asObservable();
  }

  get RecordLimitReached(): Observable<RecordProgress> {
    return this._videoLimit.asObservable();
  }

  get RecordedBlob(): Observable<RecordedVideoOutput> {
    return this._recorded.asObservable();
  }

  get RecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }


  recordingFailed(): Observable<string> {
    return this._recordingFailed.asObservable();
  }

  get Stream(): Observable<MediaStream> {
    return this._stream.asObservable();
  }

  startRecording(conf: any): Promise<any> {
    var browser = <any>navigator;
    if (this.recorder) {
      // It means recording is already started or it is already recording something
      return;
    }

    this._recordingTime.next('00:00');
    return new Promise((resolve, reject) => {
      browser.mediaDevices.getUserMedia(conf).then(stream => {
        this.stream = stream;
        this.record();
        resolve(this.stream);
      }).catch(error => {
        this._recordingFailed.next("");
        reject;
      });
    });
  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {
    this.recorder = new RecordRTC(this.stream, {
      type: 'video',
      mimeType: 'video/webm',
    });
    this.recorder.startRecording();
    let count = 0;
    this.interval = setInterval(
      () => {
        // const currentTime = moment();
        // const diffTime = moment.duration(currentTime.diff(this.startTime));
        this._stream.next(this.stream);
        count++;
        this._recordingTime.next(String(count));
        this._videoLimit.next({ progress: count, final: this.maxRecordTime });
      },
      1000
    );
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stopRecording(this.processVideo.bind(this));

      //this.processVideo.bind(this.recorder)
      //this.processVideo(this.recorder);
      //this.stopMedia();
    }
  }

  private processVideo(audioVideoWebMURL) {
    // console.log(audioVideoWebMURL);
    const recordedBlob = this.recorder.getBlob();
    const recordedName = encodeURIComponent('video_' + new Date().getTime() + '.webm');
    this._recorded.next({ blob: recordedBlob, url: audioVideoWebMURL, title: recordedName });
    this.stopMedia();
    //this.recorder.save(recordedName);
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval);
      this.startTime = null;
      if (this.stream) {
        this.stream.getAudioTracks().forEach(track => track.stop());
        this.stream.getVideoTracks().forEach(track => track.stop());
        this.stream.stop();
        this.stream = null;
      }
    }
  }
}
