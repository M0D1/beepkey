import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BreadCrumbService {
  public emitter: EventEmitter<string> = new EventEmitter();
  constructor() { }

  setTitle(title: string) {
    this.emitter.next(title);
  }

}
