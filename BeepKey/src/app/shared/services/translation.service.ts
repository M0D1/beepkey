import { EventEmitter, Injectable } from '@angular/core';
export const LANG_RU = 'ru';
export const LANG_EN = 'en';

const APP_LANGUAGE: string = 'lang';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {
  langEE: EventEmitter<string> = new EventEmitter();

  constructor() { }

  get language() {
    return localStorage.getItem(APP_LANGUAGE);
  }

  set language(value: string) {
    if (value && value !== this.language) {
      localStorage.setItem(APP_LANGUAGE, value);
      this.changeLanguage();
    }
  }

  changeLanguage() {
    this.langEE.next(this.language);
    window.location.reload();
  }
}
