import { User } from "./user.model";

export enum ProposalRank {
    not_fit = -1,
    average = 0,
    good = 1,
}
export class Proposal {
    id: number;
    rank: ProposalRank;
    extra: string;
    createdBy: User;
}