import { Company } from "./company.model";
import { Profile } from "./profile.model";

export class User {
    id: string;
    name: string;
    surname: string;
    email: string;
    company: Company;
    profile: Profile;
}