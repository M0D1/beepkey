export interface Company {
    id: number,
    name: string;
    intro: string;
    logo: string;
    link: string;
    city: string;
    country: string;
    phone: string;
}