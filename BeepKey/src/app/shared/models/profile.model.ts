export interface Profile {
    id: number;
    avatar: string;
    city: string;
    country: string;
    state: string;
    startDate: Date | undefined | null;
    endDate: Date | undefined | null;
}