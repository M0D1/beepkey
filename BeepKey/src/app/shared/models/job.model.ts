import { Company } from "./company.model";
import { Proposal } from "./proposal.model";
export enum EmploymentType {
    FullTime = 0,
    PartTime = 1,
    Volunteering = 2,
    OneTimeTask = 3
}
export enum ExpYears {
    Exp1 = 0,// 1 -3 Years
    Exp2 = 1,//3-7 Years
    Exp3 = 2,// + 7 Years
    Any = 3// Any
}
export enum JobStatus {
    Active,
    Inactive,
    Archived,
    Completed
}

export enum IndustryFields {
    Field1,
    Field2,
    Field3
}

export interface Job {
    id: number;
    title: string;
    payRate: number;
    company: Company;
    description: string;
    createdAt: Date;
    industry: IndustryFields;
    status: JobStatus;
    e_type: EmploymentType;
    exp: ExpYears;
    country: string;
    city: string;
    proposals: Proposal[];
    extra: {};
}