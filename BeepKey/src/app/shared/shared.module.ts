import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatSliderModule } from '@angular/material/slider';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { RecordComponent } from './components/record/record.component';
import { TranslateModule } from '@ngx-translate/core';
import { TranslationService } from './services/translation.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from "@angular/router";
import { FooterComponent } from './components/footer/footer.component';
import { SwiperModule } from 'swiper/angular';
import { BreadCrumbsComponent } from './components/bread-crumbs/bread-crumbs.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatRadioModule } from '@angular/material/radio';
import { MatIconModule } from '@angular/material/icon';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [
    RecordComponent,
    NavbarComponent,
    FooterComponent,
    BreadCrumbsComponent
  ],
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatProgressBarModule,
    FormsModule,
    RouterModule,
    MatRadioModule,
    SwiperModule,
    MatIconModule,
  ],
  providers: [],
  exports: [
    MatSliderModule,
    MatProgressBarModule,
    NavbarComponent,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,

    FooterComponent,
    SwiperModule,
    BreadCrumbsComponent,
    MatSelectModule,
    MatStepperModule,
    RecordComponent,
    MatAutocompleteModule,
    MatRadioModule,
    MatIconModule,
    NgbDropdownModule,
    MatListModule,
  ]
})
export class SharedModule { }
