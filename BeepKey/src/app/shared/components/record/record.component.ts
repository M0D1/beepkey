import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, NgZone, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as  RecordRTC from "recordrtc";
import { VideoRecordingService } from '../../services/video-recording.service';
declare var fx: any;
@Component({
  selector: 'app-record',
  templateUrl: './record.component.html',
  styleUrls: ['./record.component.scss']
})
export class RecordComponent implements OnInit {
  filter: string;
  video: any;
  isPlaying = false;
  displayControls = true;
  isVideoRecording = false;
  videoRecordedTime;
  videoBlobUrl;
  videoBlob;
  videoName;
  videoStream: MediaStream;
  videoConf = {
    video: { facingMode: "user", width: 480 },
    audio: {
      mandatory: {
        echoCancellation: false,
        googAutoGainControl: false,
        googNoiseSuppression: false,
        googHighpassFilter: false
      },
    }
  }
  maxVideoLength: number = 20;
  canvas: any;
  @ViewChild('videoElement', { static: false }) videoElement: ElementRef;
  @ViewChild('effectPreview', { static: false }) effectPreview: ElementRef;

  constructor(
    private ref: ChangeDetectorRef,
    private videoRecordingService: VideoRecordingService,
    private sanitizer: DomSanitizer
  ) {
    this.videoRecordingService.recordingFailed().subscribe(() => {
      this.isVideoRecording = false;
      this.ref.detectChanges();
    });

    this.videoRecordingService.RecordedTime.subscribe((time) => {
      this.videoRecordedTime = Number(time);
  
      this.ref.detectChanges();
    });

    this.videoRecordingService.Stream.subscribe((stream) => {
      this.videoStream = stream;
      this.ref.detectChanges();
    });

    this.videoRecordingService.RecordedBlob.subscribe((data) => {
      this.videoBlob = data.blob;
      this.videoName = data.title;
      this.videoBlobUrl = this.sanitizer.bypassSecurityTrustUrl(data.url);
      this.ref.detectChanges();
    });

    this.videoRecordingService.RecordLimitReached.subscribe(data => {
      data.final === data.progress ? this.stopVideoRecording() : null;

    });
  }

  ngOnInit(): void {

  }

  applyFilter(filter: String) {
    let videoStatus = 'play';
    this.effectPreview.nativeElement.innerHTML = '';
    var canvas = fx.canvas();
    var texture = canvas.texture(this.video);
    this.effectPreview.nativeElement.appendChild(canvas);
    setInterval(() => {
      if (this.video) {
        texture.loadContentsOf(this.video);
        if (videoStatus === 'play')
          this.drawEffect(canvas, texture, Number(filter))
      }
    }, 10);

    this.video.addEventListener('pause', () => {
      videoStatus = 'pause'
      console.log(`video pause`);
    })

    this.video.addEventListener('stop', () => {
      videoStatus = 'stop';
      console.log(`video stopped`);
    })

    this.video.play();
  }

  getRandomFloat(min, max) {
    return Math.random() * (max - min) + min;
  }

  ngAfterViewInit(): void {
    this.video = this.videoElement.nativeElement;
  }

  drawEffect(canvas, texture, effect: number) {
    switch (effect) {
      case 1:
        canvas.draw(texture).swirl(canvas.width / 2, canvas.height / 2, 100, -2).ink(0.5).update();
        break;
      case 2:
        canvas.draw(texture).brightnessContrast(-0.1, 0).update();
        break;
      case 3:
        canvas.draw(texture).hexagonalPixelate(320, 239.5, 10).update();
        break;
      case 4:
        canvas.draw(texture).lensBlur(2, 0.75, 0).update();
        break;
      case 5:
        canvas.draw(texture).hueSaturation(this.getRandomFloat(-1, 1), this.getRandomFloat(-1, 1)).update();
        break;
      default:

        break;
    }
  }
  ngOnDestroy(): void { }
  get progress() {
    return Math.floor((this.videoRecordedTime / this.maxVideoLength) * 100) | 0;
  }
  get stopRecording() {
    return this.isVideoRecording && !this.videoBlobUrl;
  }

  get startRecording() {
    return !this.isVideoRecording && !this.videoBlobUrl;
  }

  get redoRecording() {
    return !this.isVideoRecording && this.videoBlobUrl
  }

  startVideoRecording() {
    if (!this.isVideoRecording) {
      this.video.controls = false;
      this.isVideoRecording = true;
      this.videoRecordingService.maxRecordTime = this.maxVideoLength;
      this.videoRecordingService.startRecording(this.videoConf)
        .then(stream => {
          // this.video.src = window.URL.createObjectURL(stream);
          this.video.srcObject = stream;
          this.video.play();
        })
        .catch(function (err) {
          console.log(err.name + ": " + err.message);
        });
    }
  }

  abortVideoRecording() {
    if (this.isVideoRecording) {
      this.isVideoRecording = false;
      this.videoRecordingService.abortRecording();
      this.video.controls = false;
    }
  }

  stopVideoRecording() {
    if (this.isVideoRecording) {
      this.videoRecordingService.stopRecording();
      this.video.srcObject = this.videoBlobUrl;
      this.isVideoRecording = false;
      this.video.controls = true;
    }
  }

  clearVideoRecordedData() {
    this.videoBlobUrl = null;
    this.video.srcObject = null;
    this.video.controls = false;
    this.ref.detectChanges();
  }

  downloadVideoRecordedData() {
    this._downloadFile(this.videoBlob, 'video/mp4', this.videoName);
  }


  _downloadFile(data: any, type: string, filename: string): any {
    const blob = new Blob([data], { type: type });
    const url = window.URL.createObjectURL(blob);
    //this.video.srcObject = stream;
    //const url = data;
    const anchor = document.createElement('a');
    anchor.download = filename;
    anchor.href = url;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }


}
