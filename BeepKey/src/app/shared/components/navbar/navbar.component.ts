import { Component, NgModule, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { map, Observable } from 'rxjs';
import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  loginFG: FormGroup;
  loginProgress: boolean = false;
  authorized: Observable<boolean>;
  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private ngModal: NgbModal
  ) {
    this.authorized = this.auth.hasAccess().pipe(map(res => res ? true : false));

  }

  ngOnInit(): void {
    this.loginFG = this.fb.group({
      email: ['email@domain.com', Validators.compose([Validators.email, Validators.required])],
      password: ['xxxxxxxx', Validators.compose([Validators.required, Validators.minLength(8)])]
    })
    this.authorized.subscribe(res => console.log(res))
  }

  get user() {
    return this.auth.user;
  }

  get email(): AbstractControl {
    return this.loginFG.get('email')
  }

  get password(): AbstractControl {
    return this.loginFG.get('password')
  }

  open(content) {
    this.ngModal.open(content, { centered: true })
  }

  login() {
    if (this.loginFG.valid) {
      this.loginProgress = true;
      this.auth.login({ email: this.email.value, password: this.password.value }).subscribe(res => {
        this.ngModal.dismissAll();
        this.loginProgress = false;
      })
    }
  }
}
