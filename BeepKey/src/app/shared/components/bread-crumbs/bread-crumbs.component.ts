import { Component, Input, OnInit } from '@angular/core';
import { BreadCrumbService } from '../../services/bread-crumb.service';

@Component({
  selector: 'bread-crumbs',
  templateUrl: './bread-crumbs.component.html',
  styleUrls: ['./bread-crumbs.component.scss']
})
export class BreadCrumbsComponent implements OnInit {
  title: string = 'Просмотр вакансии';
  constructor(private BCS: BreadCrumbService) {
    this.BCS.emitter.subscribe(title => this.title = title)
  }

  ngOnInit(): void {
  }

}
