import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AuthService } from '../auth/services/auth.service';
@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent implements OnInit {

  constructor(private auth: AuthService, private translate: TranslateService) { }
  ngOnInit(): void {

  }
  get isAuthenticated() {
    return this.auth.isAuthenticated;
  }
}
