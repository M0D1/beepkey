import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { JobsService } from 'src/app/base/services/jobs.service';
export enum ExtraType {
  text = 'text',
  video = 'video'
}

export class JobExtra {
  title: string;
  type: ExtraType;
  length: number;
}
@Component({
  selector: 'job-additional',
  templateUrl: './additional-questions.component.html',
  styleUrls: ['./additional-questions.component.scss']
})
export class AdditionalQuestionsComponent implements OnInit {

  extraFG: FormGroup;
  tempFb: FormGroup;

  constructor(
    private fb: FormBuilder,
    private ngbModal: NgbModal,
    private jobService: JobsService
  ) { }

  ngOnInit(): void {
    this.initTempFG();
  }
  get answerType() {
    return ExtraType
  }

  initTempFG() {
    this.tempFb = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      length: ['', Validators.compose([Validators.min(100)])]
    })
  }


  newJobExtra(title?: string, type?: ExtraType, length?: number): JobExtra {
    return {
      title: title || '',
      type: type || ExtraType.text,
      length: length || 0
    }
  }
  get introQuestions() {
    return this.jobService.introQ;
  }
  get expQuestions() {
    return this.jobService.expQ;
  }
  addIntro(intro, intro_: boolean) {
    this.ngbModal.open(intro, { centered: true }).result.then(res => {
      if (this.tempFb.valid) {
        console.log('validForm');
        if (intro_) {
          this.introQuestions.push(this.newJobExtra(this.tempFb.get('title').value, this.getValueOf('type'), this.getValueOf('length')));
        } else {
          this.expQuestions.push(this.newJobExtra(this.tempFb.get('title').value, this.getValueOf('type'), this.getValueOf('length')));

        }
        this.initTempFG();
      } else {
        console.log('invalid');
      }
    });
  }

  getValueOf(fc: string) {
    return this.tempFb.get(fc).value
  }


}
