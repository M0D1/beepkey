import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BreadCrumbService } from 'src/app/shared/services/bread-crumb.service';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss']
})
export class JobDetailComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private BS: BreadCrumbService) { }

  ngOnInit(): void {
    this.BS.emitter.next("Job Details");
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
    })
  }
}