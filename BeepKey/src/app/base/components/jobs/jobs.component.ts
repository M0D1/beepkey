import { Component, OnInit } from '@angular/core';
import { BreadCrumbService } from 'src/app/shared/services/bread-crumb.service';
import { JobsService } from '../../services/jobs.service';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss']
})
export class JobsComponent implements OnInit {

  constructor(private jobService: JobsService, private BS: BreadCrumbService) { }

  ngOnInit(): void {
    this.BS.emitter.next('Просмотр вакансии')
  }

  get jobs() {
    return this.jobService.jobs;
  }
}
