import { Component, Input, OnInit } from '@angular/core';
import { Job } from 'src/app/shared/models/job.model';
import { ProposalRank } from 'src/app/shared/models/proposal.model';

@Component({
  selector: 'app-my-job',
  templateUrl: './my-job.component.html',
  styleUrls: ['./my-job.component.scss']
})
export class MyJobComponent implements OnInit {
  @Input('job') private job$: Job;
  constructor() { }

  ngOnInit(): void {
  }

  get job() {
    return this.job$;
  }

  getProposalRank(status: ProposalRank) {
    return this.job.proposals.filter(proposal => proposal.rank === status).length || 0;
  }

  get rank() {
    return ProposalRank;
  }
}
