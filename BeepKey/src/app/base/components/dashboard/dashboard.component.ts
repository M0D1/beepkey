import { Component, OnInit } from '@angular/core';
import { BreadCrumbService } from 'src/app/shared/services/bread-crumb.service';
import { JobsService } from '../../services/jobs.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private jobService: JobsService, private BS: BreadCrumbService) { }

  ngOnInit(): void {
    this.BS.emitter.next("Dashboard");
  }

  get jobs() {
    return this.jobService.myJobs
  }
  
}
