import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JobsService } from 'src/app/base/services/jobs.service';
import { Job } from 'src/app/shared/models/job.model';
import { Proposal } from 'src/app/shared/models/proposal.model';
import { User } from 'src/app/shared/models/user.model';
import { BreadCrumbService } from 'src/app/shared/services/bread-crumb.service';

@Component({
  selector: 'app-proposals',
  templateUrl: './proposals.component.html',
  styleUrls: ['./proposals.component.scss']
})
export class ProposalsComponent implements OnInit {
  jobId: number = 0;
  user: User;
  job$: Job;
  constructor(
    private jobService: JobsService,
    private activatedRoute: ActivatedRoute,
    private BS: BreadCrumbService
  ) { }

  ngOnInit(): void {
    this.BS.emitter.next("Просмотр откликов")
    this.activatedRoute.queryParams.subscribe(res => {
      this.jobId = Number(res.id)
      this.jobService.getProposal(this.jobId).subscribe(res => {
        console.log(res);
        this.job$ = res;
      })
    })
  }

  get job() {
    return this.job$;
  }

  get proposals(): Proposal[] {
    return this.job.proposals || [];
  }

}
