import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseComponent } from './base.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProposalsComponent } from './components/dashboard/proposals/proposals.component';
import { HomeComponent } from './components/home/home.component';
import { JobCreateComponent } from './components/jobs/job-create/job-create.component';
import { JobDetailComponent } from './components/jobs/job-detail/job-detail.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { ProposalComponent } from './components/proposal/proposal.component';

const routes: Routes = [
  {
    path: '',
    component: BaseComponent,
    children: [
      { path: '', redirectTo: 'jobs', pathMatch: 'full' },
      // { path: 'home', component: HomeComponent },
      { path: 'jobs', component: JobsComponent },
      { path: 'job', component: JobDetailComponent },
      { path: 'job/proposals', component: ProposalsComponent },
      { path: 'job/new', component: JobCreateComponent },
      { path: 'proposal', component: ProposalComponent },
      { path: 'dashboard', component: DashboardComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BaseRoutingModule { }
