import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseRoutingModule } from './base-routing.module';
import { BaseComponent } from './base.component';
import { SharedModule } from '../shared/shared.module';
import { TranslateModule } from '@ngx-translate/core';
import { JobsComponent } from './components/jobs/jobs.component';
import { JobDetailComponent } from './components/jobs/job-detail/job-detail.component';
import { JobItemComponent } from './components/jobs/job-item/job-item.component';
import { ProposalComponent } from './components/proposal/proposal.component';
import { JobCreateComponent } from './components/jobs/job-create/job-create.component';
import { GeneralInfoComponent } from './components/jobs/job-create/general-info/general-info.component';
import { AdditionalQuestionsComponent } from './components/jobs/job-create/additional-questions/additional-questions.component';
import { JobDescComponent } from './components/jobs/job-create/job-desc/job-desc.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MyJobComponent } from './components/dashboard/my-job/my-job.component';
import { ProposalsComponent } from './components/dashboard/proposals/proposals.component';
import { HomeComponent } from './components/home/home.component';

import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
  declarations: [
    BaseComponent,
    JobsComponent,
    JobDetailComponent,
    JobItemComponent,
    ProposalComponent,
    JobCreateComponent,
    GeneralInfoComponent,
    AdditionalQuestionsComponent,
    JobDescComponent,
    DashboardComponent,
    MyJobComponent,
    ProposalsComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    BaseRoutingModule,
    TranslateModule.forChild(),
    SharedModule,
    MatProgressBarModule
  ]
})
export class BaseModule { }
