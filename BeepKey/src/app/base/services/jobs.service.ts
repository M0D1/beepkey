import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { tap } from 'rxjs';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Job } from 'src/app/shared/models/job.model';
import { UrlBuilder } from 'src/app/shared/utils/url.builder';
import { JobExtra } from '../components/jobs/job-create/additional-questions/additional-questions.component';

@Injectable({
  providedIn: 'root'
})
export class JobsService {

  jobs: Job[] = [];
  introQ: JobExtra[] = [];
  expQ: JobExtra[] = [];
  constructor(
    private auth: AuthService,
    private http: HttpClient,
    private url: UrlBuilder,

  ) {
    this.getJobs().subscribe();
  }

  initForm() {

  }
  getJobs() {
    return this.http.get<Job[]>(this.url.job(''), { headers: this.auth.accessHeader }).pipe(tap(res => this.jobs = res));
  }

  getProposal(id: number) {
    return this.http.get<Job>(this.url.job(String(id)), { headers: this.auth.accessHeader });
  }

  get myJobs() {
    return this.jobs.filter(job => job.company.id === this.auth.myCompany.id) || null;
  }

  createJob() {

    const extra = { intro: this.introQ, exp: this.expQ };

    return this.http.post(this.url.job(), {}, { headers: this.auth.accessHeader });
  }


}
