export enum ExtraType {
    text = 'text',
    video = 'video'
}

export class JobExtra {
    title: string;
    type: ExtraType;
    length?: number;
}