export enum JobStatus {
    Active,
    Inactive,
    Archived,
    Completed
}