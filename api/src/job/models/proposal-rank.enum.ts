export enum ProposalRank {
    not_fit = -1,
    average = 0,
    good = 1,
}