export enum EmploymentType {
    FullTime = 0,
    PartTime = 1,
    Volunteering = 2,
    OneTimeTask = 3
}