import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { join } from 'path/posix';
import { UserPayload } from 'src/auth/models/user.payload';
import { UserService } from 'src/user/services/user.service';
import { Repository } from 'typeorm';
import { CreateProposalDTO } from '../dto/create-proposal.dto';
import { ProposalEntity } from '../entity/proposal.entity';
import { JobService } from '../job.service';

@Injectable()
export class ProposalService {
    async saveImage(file: Express.Multer.File, jobId: number) {
        throw new Error('Method not implemented.');
    }

    constructor(
        @InjectRepository(ProposalEntity)
        private repo: Repository<ProposalEntity>,
        private jobService: JobService,
        private userService: UserService
    ) {
    }

    async create(jobId: number, user: UserPayload, data: CreateProposalDTO) {
        const job = await this.jobService.findOne(jobId);
        const applicant = await this.userService.findOne(user.id);
        const proposal: ProposalEntity = new ProposalEntity();
        proposal.createdBy = applicant;
        proposal.job = job;
        // proposal.
    }

    async getProposal(jobId: number, user: UserPayload) {
        throw new Error('Method not implemented.');
    }

    async getProposals(jobId: number, user: UserPayload) {
        const job = await this.jobService.findOne(jobId);
        return await this.repo.find({ where: { job: job } });
    }
}
