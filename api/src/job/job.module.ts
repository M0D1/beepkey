import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from 'src/user/user.module';
import { JobEntity } from './entity/job.entity';
import { JobController } from './controllers/job.controller';
import { JobService } from './job.service';
import { ProposalController } from './controllers/proposal.controller';
import { ProposalService } from './proposal/proposal.service';
import { ProposalEntity } from './entity/proposal.entity';

@Module({
  imports: [TypeOrmModule.forFeature([JobEntity, ProposalEntity]), UserModule],
  controllers: [JobController, ProposalController],
  providers: [JobService, ProposalService]
})
export class JobModule { }
