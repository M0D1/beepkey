import { UserEntity } from "src/user/entity/user.entity";

export const Capitalize = (word: string) => {
  return word.charAt(0).toUpperCase() + word.slice(1);
};
export const ownerDeconstruct = (ownerEntity: UserEntity) => {
  return `${ownerEntity.name} ${ownerEntity.surname}`;
};
