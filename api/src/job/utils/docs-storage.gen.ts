import { diskStorage } from 'multer';
import * as fs from 'fs';
import { editFileName } from './edit-file-name';

export const storage = {
  storage: diskStorage({
    destination: (req, file, cb) => {
      const baseDirectory = `./uploads/`;

      if (!fs.existsSync(baseDirectory)) {
        fs.mkdirSync(baseDirectory, { recursive: true });
      }
      return cb(null, baseDirectory);
    },
    filename: editFileName,
  }),
};
