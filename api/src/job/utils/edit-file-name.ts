import * as path from 'path';
import * as UUID from 'uuid'
export const editFileName = (req, file: Express.Multer.File, callback) => {
  const fileExtName = path.extname(file.originalname);
  callback(null, `${UUID.v4()}${fileExtName}`);
};
