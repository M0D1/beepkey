import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { join } from 'path/posix';
import { UserPayload } from 'src/auth/models/user.payload';
import { CompanyService } from 'src/user/services/company.service';
import { UserService } from 'src/user/services/user.service';
import { json } from 'stream/consumers';
import { Repository } from 'typeorm';
import { CreateJobDTO } from './dto/create-job.dto';
import { JobEntity } from './entity/job.entity';

@Injectable()
export class JobService {

    constructor(
        @InjectRepository(JobEntity)
        private readonly repo: Repository<JobEntity>,
        private companyService: CompanyService,
        private userService: UserService
    ) { }

    async create(user: UserPayload, data: CreateJobDTO) {
        const { city, country, description, e_type, exp, extra, industry, payRate, title } = data;
        const company = await this.companyService.findOne(user);
        const job: JobEntity = new JobEntity();
        job.title = title;
        job.country = country;
        job.company = company;
        job.city = city;
        job.description = description;
        job.e_type = e_type;
        job.exp = exp;
        job.extra = extra ? JSON.stringify(extra) : '';
        job.industry = industry;
        job.payRate = payRate;

        return await job.save();
    }
    async fetch() {
        return await this.repo.find();
    }

    async findOne(id: number) {
        const found = await this.repo.findOne({ id })
        if (!found) {
            throw new NotFoundException('error.job_not_found')
        }
        return found;
    }

    async update() { }

    async delete(id: number) { }
}
