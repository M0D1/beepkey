import { ApiProperty } from "@nestjs/swagger";
import { IsEnum, IsNotEmpty, IsNumber, IsOptional } from "class-validator";
import { EmploymentType } from "../models/employment-type.enum";
import { ExpYears } from "../models/experience.enum";
import { IndustryFields } from "../models/industry.enum";

export class CreateJobDTO {

    @ApiProperty({ name: 'title', type: 'string', description: 'Brief title about the job', examples: ['NestJs Developer required', 'Angular Developer required', 'Full-Stack Developer required'] })
    @IsNotEmpty()
    title: string;

    @ApiProperty({ type: 'string', examples: ['Moscow', 'Rostov-on-Don'] })
    @IsNotEmpty()
    city: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string', examples: ['Russia', 'Palestine'] })
    country: string;

    @IsNotEmpty()
    @IsEnum(ExpYears)
    @ApiProperty({ type: 'enum', enum: ExpYears })
    exp: ExpYears;

    @IsNotEmpty()
    @IsEnum(EmploymentType)
    @ApiProperty({ type: 'enum', enum: EmploymentType })
    e_type: EmploymentType;

    @IsEnum(IndustryFields)
    @IsNotEmpty()
    @ApiProperty({ type: 'enum', enum: IndustryFields })
    industry: IndustryFields;

    @IsNotEmpty()
    @IsNumber()
    @ApiProperty({ type: 'number', description: 'Amount to pay for completing the job', example: '100' })
    payRate: number;

    @ApiProperty({ type: 'string', example: 'Looking for an experienced developer for a 3–6-month project. You will work with a team of international experts for this project. This contract includes multiple sub-projects. Must be experienced with Javascript, AngularJS, Bootstrap, and Kendo UI. Please note we are creating a Rich Internet Application, not a website/blog/etc. We have specifications available for applicants to review upon request.' })
    @IsNotEmpty()
    description: string;

    @ApiProperty({ type: 'string', default: '{}', example: '{\"type\": \"example\"}' })
    @IsOptional()
    extra: string;
}