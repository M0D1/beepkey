import { ProposalEntity } from "../entity/proposal.entity";

export class CreateProposalDTO {
    extra: any;
    content: string;
}