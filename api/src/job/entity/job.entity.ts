import { ApiProperty } from "@nestjs/swagger";
import { IsOptional } from "class-validator";
import { CompanyEntity } from "src/user/entity/company.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { EmploymentType } from "../models/employment-type.enum";
import { ExpYears } from "../models/experience.enum";
import { IndustryFields } from "../models/industry.enum";
import { JobStatus } from "../models/job-status.enum";
import { ProposalEntity } from "./proposal.entity";
@Entity('jobs')
export class JobEntity extends BaseEntity {
    @IsOptional()
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar' })
    @ApiProperty({
        name: 'title', type: 'string',
        description: 'Brief title about the job',
        examples: [
            'NestJs Developer required',
            'Angular Developer required',
            'Full-Stack Developer required'
        ]
    })
    title: string;

    @Column({ type: 'varchar' })
    @ApiProperty({ type: 'string', examples: ['Moscow', 'Rostov-on-Don'] })
    city: string;

    @Column({})
    @ApiProperty()
    country: string;

    @Column({ type: 'enum', enum: ExpYears })
    @ApiProperty({ type: 'enum', enum: ExpYears })
    exp: ExpYears;

    @Column({ type: 'enum', enum: EmploymentType })
    @ApiProperty({ type: 'enum', enum: EmploymentType })
    e_type: EmploymentType;

    @Column({ type: 'enum', enum: JobStatus, default: JobStatus.Active })
    @ApiProperty({ type: 'enum', enum: JobStatus, default: JobStatus.Active })
    status: JobStatus;

    @Column({ type: 'enum', enum: IndustryFields })
    @ApiProperty({ type: 'enum', enum: IndustryFields })
    industry: IndustryFields;

    //Float
    @Column({ type: 'float' })
    @ApiProperty({ type: 'number', description: 'Amount to pay for completing the job', example: '100' })
    payRate: number;

    @Column({ type: 'text' })
    @ApiProperty({ type: 'string', example: 'Looking for an experienced developer for a 3–6-month project. You will work with a team of international experts for this project. This contract includes multiple sub-projects. Must be experienced with Javascript, AngularJS, Bootstrap, and Kendo UI. Please note we are creating a Rich Internet Application, not a website/blog/etc. We have specifications available for applicants to review upon request.' })
    description: string;

    @Column({ type: 'text' })
    @ApiProperty({ type: 'string', default: '{}', example: '{\"type\": \"example\"}' })
    extra: string;

    @ApiProperty()
    @CreateDateColumn()
    createdAt: Date;

    @ApiProperty()
    @UpdateDateColumn()
    updatedAt: Date;

    // Relations 
    @ManyToOne(() => CompanyEntity, company => company.vacancies, { eager: true })
    company!: CompanyEntity;

    @OneToMany(() => ProposalEntity, proposal => proposal.job, { eager: true })
    proposals: Array<ProposalEntity>;

}