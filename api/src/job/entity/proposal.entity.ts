import { UserEntity } from "src/user/entity/user.entity";
import { BaseEntity, Column, CreateDateColumn, Entity, ManyToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ProposalRank } from "../models/proposal-rank.enum";
import { JobEntity } from "./job.entity";
@Entity('proposals')
export class ProposalEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;


    @Column({ type: 'longtext' })
    content: string;

    createdBy: UserEntity;

    @Column({ type: 'enum', enum: ProposalRank, nullable: true })
    rank: ProposalRank;



    @Column({ type: 'longtext' })
    extra: string;


    @CreateDateColumn()
    createdAt: Date;
    @UpdateDateColumn()
    updatedAt: Date;


    // Relations
    @ManyToOne(() => JobEntity, (job) => job.proposals)
    job!: JobEntity;

}