import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOkResponse, ApiOperation, ApiParam, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { UserPayload } from 'src/auth/models/user.payload';
import { CreateJobDTO } from '../dto/create-job.dto';
import { JobEntity } from '../entity/job.entity';
import { JobService } from '../job.service';
@ApiTags('Job')
@Controller('job')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class JobController {
    constructor(private jobService: JobService) { }

    @Get('')
    @ApiOperation({ summary: 'get jobs entry' })
    async getJobs(@GetUser() user: UserPayload) {
        return this.jobService.fetch();
    }

    @Get(':id')
    @ApiOperation({ summary: 'get job entry' })
    @ApiParam({ name: 'id', type: 'number' })
    @ApiOkResponse({ type: JobEntity, isArray: true })
    async getJob(@Param('id') id: number) {
        return this.jobService.findOne(id);
    }


    @Post()
    @ApiOperation({ summary: 'Create job entry' })
    async createJob(@GetUser() user: UserPayload, @Body() data: CreateJobDTO) {
        return this.jobService.create(user, data);
    }

    @Post(':id/update')
    @ApiOperation({ summary: 'Update job entry' })
    @ApiParam({ name: 'id', description: 'Job Id' })
    async update(@GetUser() user: UserPayload, data: any) { }

    @Delete(':id')
    @ApiParam({ name: 'id', type: 'number' })
    @ApiOperation({ summary: 'delete job entry' })
    async deleteJob() { }

}
