import { Body, Controller, Get, Param, ParseIntPipe, Post, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiParam, ApiProperty, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { UserPayload } from 'src/auth/models/user.payload';
import { CreateProposalDTO } from '../dto/create-proposal.dto';
import { ProposalService } from '../proposal/proposal.service';
import { FileInterceptor } from '@nestjs/platform-express';
import { imageFileFilter } from '../utils/image.filter';
import { storage } from '../utils/docs-storage.gen';
@ApiTags('Proposals')
@Controller('job')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class ProposalController {
    constructor(private service: ProposalService) { }
    @ApiParam({ type: 'number', name: 'jobId' })
    @Post(':jobId/proposal')
    async createProposals(
        @Param('jobId', ParseIntPipe) jobId: number,
        @GetUser() user: UserPayload,
        @Body() data: CreateProposalDTO
    ) {
        return this.service.create(jobId, user, data);
    }

    @Get(':jobId/proposal')
    @ApiOperation({ summary: 'Read job proposals' })
    async getProposals(
        @Param('jobId', ParseIntPipe) jobId: number,
        @GetUser() user: UserPayload) {
        return this.service.getProposals(jobId, user)
    }

    @Get(':jobId/proposal/:id')
    @ApiOperation({ summary: 'Read Proposal', description: 'returns the authenticated user proposal for the job ' })
    async getMyProposal(
        @Param('jobId', ParseIntPipe) jobId: number,
        @GetUser() user: UserPayload) {
        return this.service.getProposal(jobId, user)
    }

    @UseInterceptors(
        FileInterceptor('cover', {
            ...storage,
            fileFilter: imageFileFilter,
        }),
    )
    @UseGuards(/**UserIsOwnerGuard */)
    @Post(':jobId/proposal/:id')
    async uploadFile(
        @Param('jobId', ParseIntPipe) jobId: number,
        @UploadedFile() file: Express.Multer.File,
        @GetUser() user: UserPayload,
        @Body('extra') extra: string,
    ) {
        return await this.service.saveImage(file, jobId);
    }
}

