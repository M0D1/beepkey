import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { ormConfig } from './config/orm.conf';
import { UserModule } from './user/user.module';
import { JobModule } from './job/job.module';
import { MulterModule } from '@nestjs/platform-express';
import { join } from 'path';

@Module({
  imports: [
    TypeOrmModule.forRootAsync(ormConfig),
    ConfigModule.forRoot({ isGlobal: true }),
    MulterModule.registerAsync({
      useFactory: () => ({
        dest: join(__dirname, './uploads'),
      }),
    }),
    AuthModule,
    UserModule,
    JobModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule { }
