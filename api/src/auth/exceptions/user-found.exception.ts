import { BadRequestException } from '@nestjs/common';
export class DuplicateEmailException extends BadRequestException {
  constructor(error?: string) {
    super('error.email_exist', error);
  }
}
