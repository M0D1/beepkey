import { ForbiddenException } from '@nestjs/common';

export class UserPasswordNotValidException extends ForbiddenException {
  constructor(error?: string) {
    super('error.invalid_credentials', error);
  }
}
