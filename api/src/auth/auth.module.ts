import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { UserSecurityService } from './services/user-security.service';
import { AuthService } from './services/auth.service';
import { JwtModule } from '@nestjs/jwt';
import { jwtConfiguration } from 'src/config/jwt.conf';
import { JwtAuthGuard } from './guards/jwt-guard';
import { JwtStrategy } from './guards/jwt-strategy';
import { PassportModule } from '@nestjs/passport';
import { UserModule } from 'src/user/user.module';

@Module({
  imports: [
    JwtModule.registerAsync(jwtConfiguration),
    PassportModule,
    UserModule
  ],
  controllers: [AuthController],
  providers: [
    UserSecurityService,
    JwtAuthGuard,
    JwtStrategy,
    AuthService
  ]
})
export class AuthModule { }
