import { IsNotEmpty, IsEmail, MinLength, MaxLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class RequestResetCode {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({
        type: 'string',
        description: 'email',
        example: 'example@domain.com',
    })
    email: string;
}

export class VerifyResetPasswordDTO {
    @IsNotEmpty()
    @MinLength(8)
    @ApiProperty({
        type: 'string',
        description: 'new password',
        example: '********',
    })
    password: string;
}

export class CodeCompareDTO extends RequestResetCode {
    @IsNotEmpty()
    @ApiProperty({
        type: 'string',
        nullable: false,
        description: 'Code',
        example: 'xxxxxxxx',
    })
    code: string;
}
