import { IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
export class EmailDTO {
    @IsNotEmpty()
    @IsEmail()
    @ApiProperty({ type: 'string', example: "email@domain.com" })
    email: string;
}