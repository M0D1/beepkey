import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsEnum, IsNotEmpty, IsOptional, MinLength } from 'class-validator';
import { AccountType } from '../models/account-type.enum';

export class LoginDTO {
    @IsEmail()
    @IsNotEmpty()
    @ApiProperty({ type: 'string', title: 'Email Address', example: 'email@domain.com' })
    email: string;

    @IsNotEmpty()
    @MinLength(8)
    @ApiProperty({ type: 'string', title: 'Password', example: 'xxxxxxxx', minLength: 8, maxLength: 256 })
    password: string;


}

export class RegisterDTO extends LoginDTO {
    @IsNotEmpty()
    @IsEnum(AccountType)
    @ApiProperty({ type: 'enum', enum: AccountType, nullable: false })
    readonly role: AccountType;

    @IsNotEmpty()
    @ApiProperty({ type: 'string', title: 'First name', example: 'Jone', maxLength: 256, })
    name: string;

    @IsNotEmpty()
    @ApiProperty({
        type: 'string', title: 'Last name', example: 'Doe', required: true, maxLength: 256,
    })
    surname: string;

    @IsOptional()
    @ApiProperty({ type: 'string', title: 'Company name', example: 'BeepKey Inc.', maxLength: 256, required: false })
    company_name: string;
}
