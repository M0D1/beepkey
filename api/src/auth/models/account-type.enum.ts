export enum AccountType {
    Freelancer = 'freelancer',
    Company = 'company',
}
