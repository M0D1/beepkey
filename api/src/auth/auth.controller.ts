import { Body, Controller, Post } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { LoginDTO, RegisterDTO } from './dto/auth.dto';
import { UserSecurityService } from './services/user-security.service';
@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
    constructor(
        private userSecurity: UserSecurityService
    ) { }

    @Post('login')
    @ApiOperation({ summary: 'User Login' })
    async login(@Body() data: LoginDTO) {
        return this.userSecurity.login(data)
    }

    @Post('register')
    @ApiOperation({ summary: 'User Registration' })
    async registerCompany(@Body() data: RegisterDTO) {
        return this.userSecurity.register(data);
    }

}
