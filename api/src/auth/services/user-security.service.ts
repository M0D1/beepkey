import { Injectable } from '@nestjs/common';
import { CompanyEntity } from 'src/user/entity/company.entity';
import { UserProfileEntity } from 'src/user/entity/profile.entity';
import { UserEntity } from 'src/user/entity/user.entity';
import { UserService } from 'src/user/services/user.service';
import { LoginDTO, RegisterDTO } from '../dto/auth.dto';
import { UserPasswordNotValidException } from '../exceptions/user-password-not-valid.exception';
import { AccountType } from '../models/account-type.enum';
import { UserPayload } from '../models/user.payload';
import { AuthService } from './auth.service';

@Injectable()
export class UserSecurityService {

    constructor(
        private authService: AuthService,
        private userService: UserService
    ) { }

    async login(data: LoginDTO) {
        const { password, email } = data;
        const user = await this.findByEmail(email);
        const match = await this.authService.comparePasswords(
            password,
            user.password,
        );
        if (!match) {
            throw new UserPasswordNotValidException();
        }
        const userPayload: UserPayload = { id: user.id };
        const token = await this.authService.generateJWT(userPayload);

        return {
            token: token,
        };
    }

    async register(data: RegisterDTO) {
        const { role, email, name, password, surname, company_name } = data;
        try {
            const user: UserEntity = new UserEntity();
            user.password = password;
            user.email = email;
            user.name = name;
            user.surname = surname;
            const userInDB = await user.save();

            if (role === AccountType.Company) {
                const company: CompanyEntity = new CompanyEntity();
                company.name = company_name;
                company.owner = userInDB;
                company.intro = '';
                const companyInDB = await company.save();
                user.company = companyInDB;
            } else {
                const profile: UserProfileEntity = new UserProfileEntity();
                profile.user = userInDB;
                const profileInDB = await profile.save();
                user.profile = profileInDB;
            }

            await userInDB.save();
            return this.login({ email, password })
        } catch (err) {
            throw new Error(err.message)
        }

    }

    async findByEmail(email: string) {
        return await this.userService.findByEmail(email);
    }
}
