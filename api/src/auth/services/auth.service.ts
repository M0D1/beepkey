import { Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserPayload } from '../models/user.payload';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    private readonly logger = new Logger('AuthService');
    constructor(private readonly jwtService: JwtService) { }

    async generateJWT(user: UserPayload) {
        return await this.jwtService.signAsync({ user });
    }

    async comparePasswords(password: string, passwordHash: string) {
        return bcrypt.compare(password, passwordHash);
    }

    verifyUser(token: string): UserPayload | PromiseLike<UserPayload> {
        return this.jwtService.verifyAsync(token)
    }
}
