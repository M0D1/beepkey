import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { UserPayload } from '../models/user.payload';

export const GetUser = createParamDecorator(
    (data: unknown, ctx: ExecutionContext): UserPayload => {
        return ctx.switchToHttp().getRequest().user as UserPayload;
    },
);
