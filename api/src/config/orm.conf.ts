import { ConfigModule, ConfigService } from "@nestjs/config";
import { TypeOrmModuleAsyncOptions } from "@nestjs/typeorm";
import { UserEntity } from '../user/entity/user.entity';

import { JobEntity } from "../job/entity/job.entity";
import { EducationEntity } from '../user/entity/education.entity';
import { UserProfileEntity } from "../user/entity/profile.entity";
import { CompanyEntity } from "../user/entity/company.entity";
import { ProposalEntity } from "src/job/entity/proposal.entity";
export const ormConfig: TypeOrmModuleAsyncOptions = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get('DATABASE_HOST'),
        username: configService.get('DATABASE_USER'),
        password: configService.get('DATABASE_PASSWORD'),
        port: Number(configService.get('DATABASE_PORT')),
        database: configService.get('DATABASE_DB'),
        synchronize: configService.get('MODE') === 'development' ? true : false,
        entities: [
            UserEntity,
            EducationEntity,
            UserProfileEntity,
            CompanyEntity,
            JobEntity,
            ProposalEntity
        ],
    }),
};
