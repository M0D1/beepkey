import { Body, Controller, Get, Param, ParseIntPipe, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { UserPayload } from 'src/auth/models/user.payload';
import { CreateCompanyDTO } from '../dto/create-company.dto';
import { CompanyService } from '../services/company.service';
@ApiTags('Company')
@Controller('company')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth()
export class CompanyController {
    constructor(private service: CompanyService) {

    }

    @Get('my')
    @ApiOperation({ summary: 'Fetch User Company', description: 'Returns the authenticated user company' })
    async getUserCompany(@GetUser() user: UserPayload) {
        return this.service.findOne(user);
    }

    @Get(':id')
    @ApiOperation({ summary: 'Fetch Company' })
    async getCompanyById(@Param('id', ParseIntPipe) id: number) {

    }

    @Post('')
    @ApiOperation({ summary: '' })
    async createCompany(
        @GetUser() user: UserPayload,
        @Body() data: CreateCompanyDTO
    ) {
        return this.service.create(user, data);
    }

}
