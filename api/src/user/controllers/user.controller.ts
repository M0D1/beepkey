import { Controller, Get, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/auth/decorators/get-user.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { UserPayload } from 'src/auth/models/user.payload';
import { UserService } from '../services/user.service';
@ApiTags('User')
@Controller('user')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
export class UserController {
    constructor(private userService: UserService) { }

    @Get('me')
    @ApiOperation({ summary: 'Fetch personal Profile' })
    async getUserProfile(@GetUser() user: UserPayload) {
        return this.userService.me(user);
    }

}
