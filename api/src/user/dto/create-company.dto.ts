import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsOptional } from "class-validator";

export class CreateCompanyDTO {
    @IsNotEmpty()
    @ApiProperty({ name: 'name', type: 'string', description: 'Company Name', example: 'BeepKey Inc.' })
    name: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string', examples: ['Moscow', 'Rostov-on-Don'] })
    city: string;

    @IsNotEmpty()
    @ApiProperty({ type: 'string', examples: ['Russia', 'Palestine'] })
    country: string;

    @ApiProperty({ type: 'longtext', example: '' })
    @IsNotEmpty()
    intro: string;
    @IsOptional()
    @ApiProperty({ name: 'logo', description: 'Company Logo' })
    logo?: string;
    @IsOptional()
    @ApiProperty({ name: 'link', description: 'Company Site', example: 'www.example.com' })
    link?: string;
    @IsOptional()
    @ApiProperty({ name: 'phone', example: '+799911122233', required: false })
    phone?: string;

}