import { Module } from '@nestjs/common';
import { UserController } from './controllers/user.controller';
import { CompanyService } from './services/company.service';
import { UserService } from './services/user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyEntity } from './entity/company.entity';
import { UserEntity } from './entity/user.entity';
import { EducationEntity } from './entity/education.entity'
import { UserProfileEntity } from './entity/profile.entity';
import { CompanyController } from './controllers/company.controller';
@Module({
  imports: [TypeOrmModule.forFeature([CompanyEntity, UserEntity, EducationEntity, UserProfileEntity])],
  controllers: [UserController, CompanyController],
  providers: [UserService, CompanyService],
  exports: [UserService, CompanyService]
})
export class UserModule { }
