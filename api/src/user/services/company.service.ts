import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserPayload } from 'src/auth/models/user.payload';
import { Repository } from 'typeorm';
import { CreateCompanyDTO } from '../dto/create-company.dto';
import { CompanyEntity } from '../entity/company.entity';
import { UserProfileEntity } from '../entity/profile.entity';
import { UserEntity } from '../entity/user.entity';
import { UserService } from './user.service';

@Injectable()
export class CompanyService {
    private logger: Logger = new Logger(CompanyService.name);
    constructor(
        @InjectRepository(CompanyEntity)
        private repo: Repository<CompanyEntity>,
        private userService: UserService
    ) { }

    async findOne(user: UserPayload) {
        const owner = await this.userService.findOne(user.id);
        this.logger.log(owner.company)
        if (!owner.company) {
            throw new BadRequestException('error.create_company_profile');
        }

        return this.companyData(owner.company.id);
    }

    async companyData(id: number) {
        return this.repo.findOne({ where: { id: id } });
    }

    async create(user: UserPayload, data: CreateCompanyDTO) {
        const userInDB = await this.userService.findOne(user.id);
        if (userInDB.company) {
            throw new BadRequestException('error.user_have_company');
        }
        const { city, country, intro, link, logo, phone, name } = data;
        const company: CompanyEntity = new CompanyEntity();
        company.name = name;
        company.intro = intro ? intro : '';
        company.city = city ? city : '';
        company.country = country ? country : '';
        company.link = link ? link : '';
        company.logo = logo ? logo : '';
        company.phone = phone ? phone : '';
        company.owner = userInDB;
        const saved = await company.save();
        userInDB.company = saved;
        await userInDB.save();
        //   TODO 
        return { message: 'company updated' }
    }

    async update() { }
    async delete() { }
}
