import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { userInfo } from 'os';
import { UserNotFoundException } from 'src/auth/exceptions/user-not-found.exception';
import { UserPayload } from 'src/auth/models/user.payload';
import { Repository } from 'typeorm';
import { UserEntity } from '../entity/user.entity';

@Injectable()
export class UserService {

    constructor(
        @InjectRepository(UserEntity)
        private repo: Repository<UserEntity>
    ) { }


    async me(user: UserPayload) {
        return await this.findOne(user.id);
    }

    async findOne(id: string) {
        const user = await this.repo.findOne({ where: { id: id } })
        if (!user) {
            throw new UserNotFoundException()
        }
        return user;
    }

    async findByEmail(email: string) {
        const found = await this.repo.findOne({ where: { email: email.toLocaleLowerCase() } });
        if (!found)
            throw new UserNotFoundException()
        return found;
    }

    get repository() {
        return this.repo;
    }
}
