import { BaseEntity, Column, Entity, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { EducationEntity } from "./education.entity";
import { UserEntity } from "./user.entity";
@Entity('profiles')
export class UserProfileEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', default: '' })
    avatar: string;

    @Column({ type: 'varchar', default: '' })
    city: string;

    @Column({ type: 'varchar', default: '' })
    country: string;

    @Column({ type: 'varchar', default: '' })
    state: string;

    @Column({ type: 'date', nullable: true })
    startDate: Date;

    @Column({ type: 'date', nullable: true })
    endDate: Date;

    // Relations
    @OneToOne(() => UserEntity, (user) => user.profile)
    user!: UserEntity;

    @OneToMany(() => EducationEntity, education => education.profile)
    education: Array<EducationEntity>;
}