import { JobEntity } from "src/job/entity/job.entity";
import { BaseEntity, Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserEntity } from "./user.entity";
@Entity('companies')
export class CompanyEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: 'varchar', default: '' })
    name: string;

    @Column({ type: 'longtext' })
    intro: string;

    @Column({ type: 'varchar', default: '' })
    logo: string;

    @Column({ type: 'varchar', default: '' })
    link: string;
  
    @Column({ type: 'varchar', default: '' })
    city: string;
  
    @Column({ type: 'varchar', default: '' })
    country: string;
   
    @Column({ type: 'varchar', default: '' })
    phone: string;

    // Relations
    @OneToMany(() => JobEntity, job => job.company)
    vacancies: Array<JobEntity>

    @OneToOne(() => UserEntity, user => user.company)
    owner!: UserEntity;
}