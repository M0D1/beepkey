import { BaseEntity, Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserProfileEntity } from "./profile.entity";
@Entity()
export class EducationEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ type: 'varchar' })
    institution: string;
    @Column({})
    degree: string;
    @Column({ type: 'varchar' })
    major: string;
    @Column({ type: 'date' })
    startDate: Date;
    @Column({ type: 'date' })
    endDate: Date;

    // Relations
    @ManyToOne(() => UserProfileEntity, profile => profile.education)
    profile!: UserProfileEntity;
}