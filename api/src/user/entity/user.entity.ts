
import { BaseEntity, BeforeInsert, Column, CreateDateColumn, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { ApiProperty } from "@nestjs/swagger";
import { Exclude } from "class-transformer"; import * as bcrypt from 'bcrypt';
import { UserProfileEntity } from "./profile.entity";
import { CompanyEntity } from "./company.entity";
import { JobEntity } from "src/job/entity/job.entity";

@Entity('users')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ nullable: false, type: 'varchar', length: 255 })
    @ApiProperty({ type: 'string', example: 'Jon', required: true })
    name: string;

    @Column({ nullable: false, type: 'varchar', length: 255 })
    @ApiProperty({ nullable: false, required: true, example: 'Doe' })
    surname: string;

    @Column({ unique: true, nullable: false, type: 'varchar', length: 255 })
    @ApiProperty({ type: 'string', example: 'email@domain.com', nullable: false })
    email: string;

    @Exclude()
    @Column({ nullable: false, type: 'varchar', length: 255 })
    @ApiProperty({ type: 'string', nullable: false })
    password: string;

    @Exclude()
    @CreateDateColumn()
    @ApiProperty({ type: 'date', nullable: true })
    createdAt: Date;

    @Exclude()
    @UpdateDateColumn()
    @ApiProperty({ type: 'date', nullable: true })
    updatedAt: Date;

    // Relations
    @OneToOne(() => CompanyEntity, company => company.owner, { eager: true, onDelete: 'CASCADE', nullable: true })
    @JoinColumn()
    company: CompanyEntity;

    @OneToOne(() => UserProfileEntity, (position) => position.user, { eager: true, onDelete: 'CASCADE', nullable: true })
    @JoinColumn()
    profile: UserProfileEntity;
 
    @BeforeInsert()
    async beforeInsert() {
        this.name = this.name.charAt(0).toUpperCase() + this.name.slice(1);
        this.surname = this.surname.charAt(0).toUpperCase() + this.surname.slice(1);
        this.email = this.email.toLocaleLowerCase();
        const hash = bcrypt.hashSync(this.password, 12);
        this.password = hash;
    }
}